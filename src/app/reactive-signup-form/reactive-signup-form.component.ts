import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { UsernameValidators } from './username.validators';

@Component({
  selector: 'app-reactive-signup-form',
  templateUrl: './reactive-signup-form.component.html',
  styleUrls: ['./reactive-signup-form.component.css']
})
export class ReactiveSignupFormComponent {
    form = new FormGroup({
      account: new FormGroup({
        username: new FormControl('',[
          Validators.required,
          Validators.minLength(3),
          UsernameValidators.cannotContainSpace
        ], UsernameValidators.shouldBeunique),
        password: new FormControl('', Validators.required)
      })
    });

    login(){
      /*let isValid = authService.login(this.form.value);
      if (!isValid) {
        this.form.setErrors({
          invalidLogin: true
        });
      }*/
      this.form.setErrors({
        invalidLogin: true
      });
    }

    get username() {
      return this.form.get('account.username');
    }
    
}

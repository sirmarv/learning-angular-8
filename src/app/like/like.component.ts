import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent{

  constructor(public likesCount: number, public isSelected: boolean) { }
  
  onClick(){
    this.likesCount += (this.isSelected) ? -1: 1;
    this.isSelected = !this.isSelected;
  }
}

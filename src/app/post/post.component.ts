import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts: any[];
 
  
  ngOnInit(): void {
    this.service.getPosts()
    .subscribe(response => {
      this.posts = response.json();
    }, error =>{
      alert('An unexpected error occured');
      console.log(error);
    });
  }
  
  constructor(private service: PostService) {
   }

   createPost(input: HTMLInputElement){
    let post = {title: input.value};
    input.value='';
    this.service.createPost(post)
      .subscribe(response => {
        post['id'] = response.json().id;  
        this.posts.splice(0,0,post);
      }, error =>{
        alert('An unexpected error occured');
        console.log(error);
      });
   }

   updatePost(post){
     //this.http.put(this.url, JSON.stringify(post));
     this.service.updatePost(post)
      .subscribe(response => {
        console.log(response.json());
      }, error =>{
        alert('An unexpected error occured');
        console.log(error);
      });
   }

   deletePost(post){
   this.service.deletePost(354)
    .subscribe(response => {
      let index = this.posts.indexOf(post);
      this.posts.splice(index, 1);
    }, 
    (error: Response) => {
      if (error.status === 400)
        alert('This post has already been deleted');
      else {
         alert('An unexpected error occured');
      }
    });
   }

}
